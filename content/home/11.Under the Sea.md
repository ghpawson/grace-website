+++
image = "img/portfolio/uts_1.jpg"
showonlyimage = false
date = "2016-11-05T19:44:32+05:30"
title = "under the sea"
draft = false
weight = 11
url = "underthesea"
+++

Gradients and I became very close while making these designs. 

<!--more-->

This project is one of the reasons that I am so happy that I keep experimenting with different design styles and techniques rather than trying to “find my artistic voice” too soon. I am in love with the style and colours that are going on in this images. 

Naming this project “Under the Sea” led me to sing the Disney song unconsciously for most of the time that I was making these designs. Much to the annoyance of my boyfriend who unfortunately shares a work space with me. Especially since I only know about two lines of the song... sorry Santi. 

![uts_1.jpg](/img/portfolio/uts_1.jpg)

![uts_2.jpg](/img/portfolio/uts_2.jpg)

![uts_3.jpg](/img/portfolio/uts_3.jpg)
